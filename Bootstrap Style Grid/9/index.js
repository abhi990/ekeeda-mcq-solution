const video = document.querySelector('video');
const pauseBtn = document.getElementById('pause-btn');
const playBtn = document.getElementById('play-btn');

pauseBtn.addEventListener('click', () => {
  video.pause();
});

playBtn.addEventListener('click', () => {
  video.play();
});
